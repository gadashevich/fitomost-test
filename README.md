### Установка

Для корректной установки на вашем сервере должны быть установлены PHP  5.4 иили выше, 
MySQL 5.6 и выше
расширение mcrypt для PHP и Composer. Для установки сделайте следующее:

Склонировать проект на локальную машину, войти в папку проекта

```
git clone https://gadashevich@bitbucket.org/gadashevich/fitomost-test.git
cd test-app/
```

Установить все зависимости приложения через Composer

```
composer install
```

Настроить подключение к MySQL базе данных в файле config/db.php
Создать базу данных приложения, выполнив SQL-запрос в MySQL

```
CREATE DATABASE `yii` COLLATE 'utf8_general_ci'
```

Запустить скрипт генерации таблиц БД
```
php yii migrate
```

Открыть страницу в браузере: http://localhost:8000/

### Проверка
1. Открыть страницу в браузере: http://localhost:8000/
2. Залогиниться под учетной записью Администратора
Логин: admin 
Пароль: admin
3. Перейти на страницу http://localhost:8000/admin/city управление городами 
4. Перейти на страницу http://localhost:8000/admin/service управление сервисами 
5. Перейти на страницу http://localhost:8000/admin/log история изменений
6. Залогиниться под учетной записью Оператора
Логин: operator 
Пароль: operator
7.REST API посмотреть список услуг по городу http://localhost:8000/v1/service?city_id=1
Посмотреть услугу   http://localhost:8000/v1/service/1

