<?php

use yii\db\Migration;
use \tebazil\yii2seeder\Seeder;

/**
 * Handles the creation of table `{{%city}}`.
 */
class m190320_114125_create_city_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%city}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'name' => $this->string(250)
        ]);

        $seeder = new Seeder();
        $generator = $seeder->getGeneratorConfigurator();
        $faker = $generator->getFakerConfigurator();

        $seeder->table('city')->columns([
            'id', //automatic pk
            'name'=>$faker->city,
        ])->rowQuantity(10);

        $seeder->refill();

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%city}}');
    }
}
