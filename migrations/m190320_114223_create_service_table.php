<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%service}}`.
 */
class m190320_114223_create_service_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%service}}', [
            'id'        => $this->primaryKey(11)->unsigned(),
            'name'  => $this->string(250),
            'code' => $this->string(50),
            'price' => $this->decimal(11.2),
            'description' => $this->text(),
            'active' => $this->boolean()->defaultValue(0),
            'start' => $this->integer(11)->unsigned(),
            'end' => $this->integer(11)->unsigned(),
            'city_id' => $this->integer(11)->unsigned()->defaultValue(0)
        ]);
        $this->createIndex('idx-city_id','service','city_id');
        $this->addForeignKey('fk-city_id','service','city_id', 'city', 'id', 'CASCADE');


        $faker = Faker\Factory::create();

        $rows = (new \yii\db\Query())
            ->select(['id', 'name'])
            ->from('city')
            ->all();

        foreach ($rows as $row){
            $rowQuantity = rand(3,5);
            for ($i = 0;$i < $rowQuantity;$i++) {
                $columns = [
                    'name' => $faker->sentence(7, true),
                    'code' => $faker->numberBetween(1000, 99999),
                    'price' => rand(100, 500),
                    'description' => $faker->text,
                    'active' => rand(0, 1),
                    'start' => time(),
                    'end' => time() + rand(10, 20) * 24 * 60 * 60,
                    'city_id' => $row['id']
                ];
                Yii::$app->db->createCommand()->insert('service', $columns)->execute();
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%service}}');
    }
}
