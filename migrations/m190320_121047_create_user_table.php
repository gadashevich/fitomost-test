<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user}}`.
 */
class m190320_121047_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey()->unsigned(),
            'username'  => $this->string(250),
            'password' => $this->string(250),
            'name' => $this->string(250),
            'email' => $this->string(250),
            'role' => "ENUM('admin', 'operator')",
        ]);

        $sql = "INSERT INTO `user` SET 
          `username`='admin',
          `password` = 'admin',
          `name` = 'Администратор',
          `email` = 'admin@admin.adm',
          `role` = 'admin'
        ";
        \Yii::$app->db->createCommand($sql)->execute();

        $sql = "INSERT INTO `user` SET 
          `username`='operator',
          `password` = 'operator',
          `name` = 'Оператор',
          `email` = 'operator@oper.op',
          `role` = 'operator'
        ";
        \Yii::$app->db->createCommand($sql)->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
