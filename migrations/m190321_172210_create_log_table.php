<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%log}}`.
 */
class m190321_172210_create_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%log}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'model_id' =>  $this->integer(11)->unsigned(),
            'user_id' =>  $this->integer(11)->unsigned(),
            'model_name' => $this->string(50),
            'type' => "ENUM('insert', 'update')",
            'date_add' => $this->integer(11)->unsigned(),
        ]);
        $this->createIndex('idx-user_id','log','user_id');
        $this->addForeignKey('fk-user_id',     'log',    'user_id', 'user', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%log}}');
    }
}
