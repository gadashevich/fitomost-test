<?php

use yii\db\Migration;
//use \tebazil\yii2seeder\Seeder;

/**
 * Handles the creation of table `{{%log_detail}}`.
 */
class m190322_065126_create_log_detail_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%log_detail}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'log_id' =>  $this->integer(11)->unsigned(),
            'attribute' =>$this->string(50),
            'before' =>$this->text(),
            'after' =>$this->text(),
        ]);
        $this->createIndex('idx-log_id','log_detail','log_id');
        $this->addForeignKey('fk-log_id',     'log_detail',    'log_id', 'log', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%log_detail}}');
    }
}
