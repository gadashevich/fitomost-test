<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property int $id
 * @property string $name
 *
 * @property Service[] $sevices
 */
class City extends \yii\db\ActiveRecord
{

    use History;

    const CITY_SAVED = 'CITY_SAVED';
    const CITY_UPDATED = 'CITY_SAVED';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'],'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices()
    {
        return $this->hasMany(Service::class, ['city_id' => 'id']);
    }

    public function beforeSave($insert){
        parent::beforeSave($insert);
        $this->addLogDatabase($this,$insert);
        return true;
    }

}
