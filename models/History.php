<?php
/**
 * Created by PhpStorm.
 * User: boris
 * Date: 22.03.19
 * Time: 17:35
 */

namespace app\models;

use \yii\db\ActiveRecord;
use Yii;

trait History
{

    public function addLogDatabase(ActiveRecord $model,$insert = false){

        $user = Yii::$app->user;
        if($user->isGuest)
            return;

        $modelName = explode('\\',get_class($model));
        $modelName = end($modelName);

        if(!$insert) {
            $oldAttributes = $model->getOldAttributes();
            $attributes = $model->getAttributes();
            $changeProps = [];

            foreach ($attributes as $attribute => $value) {
                if ($value != $oldAttributes[$attribute]) {
                    $changeProps[] = [
                        'attribute' => $attribute,
                        'before' => $oldAttributes[$attribute],
                        'after' => $value
                    ];
                }
            }

            if (count($changeProps)) {
                $columns = [
                    'model_id' => $model->id,
                    'user_id' => $user->id,
                    'model_name' => $modelName,
                    'type' => 'update',
                    'date_add' => time(),
                ];
                Yii::$app->db->createCommand()->insert('log', $columns)->execute();
                $id = Yii::$app->db->getLastInsertID();
                if ($id) {
                    foreach ($changeProps as $prop) {
                        $columns = [
                            'log_id' => $id,
                            'attribute' => $prop['attribute'],
                            'before' => $prop['before'],
                            'after' => $prop['after'],
                        ];
                        Yii::$app->db->createCommand()->insert('log_detail', $columns)->execute();
                    }
                }
            }
        }else{
            $attributes = $model->getAttributes();
            $columns = [
                'model_id' => 0,
                'user_id' => $user->id,
                'model_name' => $modelName,
                'type' => 'insert',
                'date_add' => time(),
            ];
            Yii::$app->db->createCommand()->insert('log', $columns)->execute();
            $id = Yii::$app->db->getLastInsertID();
            if ($id) {
                foreach ($attributes as $attribute => $value) {
                    if($attribute == 'id')
                        continue;
                    $columns = [
                        'log_id' => $id,
                        'attribute' => $attribute,
                        'before' => '-',
                        'after' => $value,
                    ];
                    Yii::$app->db->createCommand()->insert('log_detail', $columns)->execute();
                }
            }
        }
    }
}