<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_detail".
 *
 * @property int $id
 * @property int $log_id
 * @property string $attribute
 * @property string $before
 * @property string $after
 *
 * @property Log $log
 */
class LogDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['log_id'], 'integer'],
            [['before', 'after'], 'string'],
            [['attribute'], 'string', 'max' => 50],
            [['log_id'], 'exist', 'skipOnError' => true, 'targetClass' => Log::className(), 'targetAttribute' => ['log_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'log_id' => 'Log ID',
            'attribute' => 'Attribute',
            'before' => 'Before',
            'after' => 'After',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLog()
    {
        return $this->hasOne(Log::class, ['id' => 'log_id']);
    }
}
