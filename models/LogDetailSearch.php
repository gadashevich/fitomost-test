<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Log;
use app\models\LogDetail;

/**
 * CitySearch represents the model behind the search form of `app\models\City`.
 */
class LogDetailSearch extends LogDetail
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id','log_id'], 'integer'],
            [['attribute', 'before', 'after'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogDetail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'log_id' =>  $this->log_id,
            'attribute' =>$this->attribute,
            'before' =>$this->before,
            'after' =>$this->after,
        ]);

        $query->andFilterWhere(['like', 'attribute', $this->attribute]);
        $query->andFilterWhere(['like', 'before', $this->before]);
        $query->andFilterWhere(['like', 'after', $this->after]);

        return $dataProvider;
    }
}
