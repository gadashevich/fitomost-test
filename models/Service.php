<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "service".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string $price
 * @property string $description
 * @property int $active
 * @property int $start
 * @property int $end
 * @property int $city_id
 *
 * @property City $city
 */
class Service extends \yii\db\ActiveRecord
{

    use History;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['name','price','description','code'],'required'],

            [['price'], 'number'],
            [['description'], 'string'],
            [['active', 'start', 'end', 'city_id'], 'integer'],
            [['name'], 'string', 'max' => 250],
            [['code'], 'string', 'max' => 50],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::class, 'targetAttribute' => ['city_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
            'price' => 'Price',
            'description' => 'Description',
            'active' => 'Active',
            'start' => 'Start',
            'end' => 'End',
            'city_id' => 'City ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }


    public function beforeSave($insert){
        parent::beforeSave($insert);
        $this->addLogDatabase($this,$insert);
        return true;
    }

}
