<?php
/**
 * Created by PhpStorm.
 * User: boris
 * Date: 21.03.19
 * Time: 11:50
 */

namespace app\modules\admin\controllers;

use Yii;

trait Check
{
    public function checkAccess($action)
    {
        $user = Yii::$app->user;

        if ($user->isGuest)
            throw new \yii\web\ForbiddenHttpException(sprintf('You can only %s lease that you\'ve created.', $action));
        if ($user->identity->role != 'admin')
            throw new \yii\web\ForbiddenHttpException(sprintf('You can only %s lease that you\'ve created.', $action));
    }
}