<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;



/* @var $this yii\web\View */
/* @var $searchModel app\models\ServiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Log Detail';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sevice-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'model_name',
//                'filter' => \app\models\Log::find()->select(['model_name','id'])->indexBy('id')->column(),
                'value' => function(\app\models\LogDetail $detail){
                    return $detail->log->model_name;
                }
            ],
            [
                'attribute' => 'type',
//                'filter' => \app\models\Log::find()->select(['model_name','id'])->indexBy('id')->column(),
                'value' => function(\app\models\LogDetail $detail){
                    return $detail->log->type;
                }
            ],
            [
                'attribute' => 'name',
//                'filter' => \app\models\Log::find()->select(['model_name','id'])->indexBy('id')->column(),
                'value' => function(\app\models\LogDetail $detail){
                    return $detail->log->user->name;
                }
            ],
            [
                'attribute' => 'date_add',
//                'filter' => \app\models\Log::find()->select(['model_name','id'])->indexBy('id')->column(),
                'value' => function(\app\models\LogDetail $detail){
                    return date('Y-m-d H:i:s',$detail->log->date_add);
                }
            ],
            'attribute',
            'before',
            'after',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
