<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


$user = \Yii::$app->user;

/* @var $this yii\web\View */
/* @var $model app\models\Sevice */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sevice-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'start')->textInput() ?>

    <?= $form->field($model, 'end')->textInput() ?>

    <?php if($user->identity->role == 'admin'){ ?>
        <div class="form-group field-service-end">
            <label class="control-label" for="active">Active</label>
            <?= Html::dropDownList('Service[active]',$model->active,[
                0 => 'No',
                1 => 'Yes'
            ],['class' => 'form-control'])?>
            <div class="help-block"></div>
        </div>
    <?php } ?>

    <div class="form-group field-service-end">
        <label class="control-label" for="active">City</label>
        <?= Html::dropDownList('Service[city_id]',$cityId,$cities,['class' => 'form-control'])?>
        <div class="help-block"></div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
