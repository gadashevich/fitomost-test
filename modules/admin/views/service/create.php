<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Sevice */

$this->title = 'Create Sevice';
$this->params['breadcrumbs'][] = ['label' => 'Sevices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sevice-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'cities' => $cities,
        'cityId' => null
    ]) ?>

</div>
