<?php

namespace app\modules\v1\controllers;

use Yii;
use yii\rest\ActiveController;
use app\models\ServiceSearch;
use app\models\Service;

class ServiceController extends ActiveController
{

    public $modelClass = Service::class;

    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        return $actions;
    }

    public function prepareDataProvider()
    {
        $searchModel = new ServiceSearch;
        return  $searchModel->search(Yii::$app->request->queryParams);
    }

}
